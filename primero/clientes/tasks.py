from celery import task
import time

@task(queue="mensajes", max_retries=3)
def enviar_mensaje(mensaje):
    time.sleep(10)
    return True

@task(queue="video", max_retries=3)
def convertir_video(video):
    try:
        #time.sleep(30)
        raise Exception("esta cosa fallo")
    except Exception, e:
        convertir_video.retry(exc=e, countdown=1)
    return True
