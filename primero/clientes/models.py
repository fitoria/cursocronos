import logging
import time

from django.db import models
from django.contrib.auth.models import User

from tasks import enviar_mensaje, convertir_video

class Cliente(models.Model):
    nombre = models.CharField(max_length=100)
    telefono = models.CharField(max_length=8)

    class Meta:
        permissions = (
                ("can_get_message", "El usuario puede recibir mensajes"),
        )

    def __unicode__(self):
        return self.nombre

    def enviar_mensaje(self, mensaje):
        '''
        Simulacion de envio de sms,
        en realidad no manda nada'''
        return enviar_mensaje.delay(mensaje)

    def convertir_video(self, video):
        return convertir_video.delay(video)

class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    costo = models.FloatField()
    fecha = models.DateField()
    agregado_por = models.ForeignKey(User)

    def __unicode__(self):
        return "Producto: %s (%s)" % (self.nombre, self.costo)

class Subscripcion(models.Model):
    fecha = models.DateField(auto_now_add=True)
    producto = models.ForeignKey(Producto)
    cliente = models.ForeignKey(Cliente)

    class Meta:
        verbose_name_plural = 'Subscripciones'
        verbose_name = "Subscripcion"
        unique_together = ['producto', 'cliente']

    def __unicode__(self):
        return "Producto: %s adquirido por: %s" % (self.producto.nombre, self.cliente.nombre)
