# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cliente'
        db.create_table('clientes_cliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=8)),
        ))
        db.send_create_signal('clientes', ['Cliente'])


    def backwards(self, orm):
        # Deleting model 'Cliente'
        db.delete_table('clientes_cliente')


    models = {
        'clientes.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '8'})
        }
    }

    complete_apps = ['clientes']