from clientes.models import Cliente

from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Manda un mensaje '
    args = '<id_usuario>'

    def handle(self, *args, **options):
        id = args[0]
        try:
            cliente = Cliente.objects.get(pk=id)
        except:
            raise CommandError("ID cliente incorrecto")

        cliente.enviar_mensaje('Favor cancele su factura')

        self.stdout.write("Mensaje Enviado a: %s" % cliente.nombre)
