from clientes.models import Cliente

from django.core.management.base import NoArgsCommand, CommandError
from optparse import make_option

TIPOS_VALIDOS = ('cancelar', 'saludo')

class Command(NoArgsCommand):
    help = 'Manda mensajes a todos'

    option_list = NoArgsCommand.option_list + (
        make_option('--tipo',
            action='store',
            dest='tipo',
            help='Tipo de mensaje'),
        )

    def handle_noargs(self, **options):
        tipo = options.get('tipo')
        if tipo  == 'cancelar':
            mensaje = 'Favor cancele su factura'
        elif tipo == 'saludo':
            mensaje = 'hola!'
        else:
            mensaje = "Saludo!"

        for cliente in Cliente.objects.all():
            cliente.enviar_mensaje(mensaje)
            self.stdout.write("Mensaje Enviado a: %s" % cliente.nombre)
