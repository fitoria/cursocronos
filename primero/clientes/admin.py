from django.contrib import admin
from django.core.mail import send_mail
from django.shortcuts import render
from django.conf.urls.defaults import patterns, include, url

from models import Cliente, Producto, Subscripcion



#class SubscripcionInline(admin.StackedInline):
class SubscripcionInline(admin.TabularInline):
    model = Subscripcion
    max_num = 2
    verbose_name = 'Manejo de subscripcion'
    verbose_name_plural = 'Manejo de subscripciones'

class ClienteAdmin(admin.ModelAdmin):
    inlines = [SubscripcionInline]

    def get_urls(self):
        urls = super(ClienteAdmin, self).get_urls()
        mis_urls = patterns('',
            url(r'^lista/$',
                self.admin_site.admin_view(self.lista_cliente),
                name='lista_clientes'),
        )
        return mis_urls + urls

    def lista_cliente(self, request):
        context = {
                   'clientes': self.queryset(request),
                   'app_label': 'cliente',
                  }
        return render(request, 'admin/clientes/lista_clientes.html', context)

class ProductoAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'agregado_por__username')
    exclude = ('agregado_por', )
    list_filter = ('agregado_por__username', 'fecha')
    list_display = ('nombre', 'costo', 'fecha', 'agregado_por')
    actions = ['aumentar_precio', 'enviar_notificacion' ]


    def save_model(self, request, obj, form, change):
        obj.agregado_por = request.user
        obj.save()

    def queryset(self, request):
        qs = super(ProductoAdmin, self).queryset(request)
        if  not request.user.is_superuser:
            return qs.filter(agregado_por = request.user)
        else:
            return qs

    def aumentar_precio(self, request, queryset):
        for producto in queryset.all():
            producto.costo += 10
            producto.save()

    def enviar_notificacion(self, request, queryset):
        for producto in queryset.all():
            email = producto.agregado_por.email
            mensaje = "Su producto %s esta listo" % producto.nombre
            send_mail('Hola', mensaje, 'hola@miapp.com', [email, ], True)

class SubscripcionAdmin(admin.ModelAdmin):
    fieldsets = (
            #(None, {'fields': (('producto', 'cliente'),) }),
            (None, {'fields': ('producto',), 'classes': ('wide',) }),
            ("Cliente", {'fields': ('cliente',), 'classes': ('extrapretty',) }),
            )

    class Media:
        js = ('algo.js', )


admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Subscripcion, SubscripcionAdmin)
